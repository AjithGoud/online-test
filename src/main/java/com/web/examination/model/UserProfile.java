package com.web.examination.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "user_profiles")
public class UserProfile {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("user_id")
	@Column(name = "user_id")
	private int userId;
	@JsonProperty("first_name")
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	@JsonProperty("last_name")
	private String lastName;
	@Column(name = "email_id")
	@JsonProperty("email_id")
	private String emailId;
	@Column(name = "username")
	@JsonProperty("username")
	private String userName;
	@Column(name = "contact_number")
	@JsonProperty("contact_number")
	private String contactNumber;
	@Column(name = "address")
	@JsonProperty("address")
	private String address;
	@Column(name = "password")
	@JsonProperty("password")
	private String password;
	@Column(name = "created_date")
	@JsonProperty("created_date")
	private Date createdDate;
	@Column(name = "created_by")
	@JsonProperty("created_by")
	private String createdBy;
	@JsonProperty("is_active")
	@Column(name = "is_active")
	private boolean isActive;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}
