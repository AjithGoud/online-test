package com.web.examination.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web.examination.model.Roles;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Integer> {

	Roles findByRole(String role);
}
