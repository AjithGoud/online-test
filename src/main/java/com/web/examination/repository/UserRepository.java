package com.web.examination.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web.examination.model.UserProfile;

@Repository
@Qualifier("userRepository")
public interface UserRepository extends JpaRepository<UserProfile, Integer> {

	UserProfile findByEmailId(String emailId);
	UserProfile findByUserName(String userName);

}
