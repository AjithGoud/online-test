package com.web.examination.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.web.examination.model.UserProfile;
import com.web.examination.repository.UserRepository;

@RestController
@RequestMapping(value = "api/v1/users")
public class UserController {
	@Autowired
	UserRepository userRepository;

	@GetMapping(value = "/findAll")
	public ResponseEntity<?> findAll() {
		List<UserProfile> users = userRepository.findAll();
		String message = null;
		if (users.isEmpty()) {
			message = new String("No Users Found");
			return ResponseEntity.ok().body(message);
		}
		return ResponseEntity.ok().body(users);
	}

	@GetMapping(value = "/findByUserName")
	public ResponseEntity<?> findUser(@RequestParam String userName) {
		UserProfile user = userRepository.findByUserName(userName);
		if (user != null) {
			return ResponseEntity.ok().body(user);
		}
		String message = new String("User Not Found");
		return ResponseEntity.ok().body(message);

	}

	@PostMapping(value = "/")
	public ResponseEntity<?> createUser(@RequestBody UserProfile user) {
		user.setCreatedDate(new Date());
		user.setCreatedBy("  ");
		user.setActive(true);
		userRepository.save(user);
		return ResponseEntity.ok().body("User Profile Created");

	}

	@DeleteMapping(value = "/")
	public ResponseEntity<?> removeUser(@RequestParam String userName) {
		UserProfile user = userRepository.findByUserName(userName);
		user.setActive(false);
		userRepository.save(user);
		return ResponseEntity.ok().body("User Profile Deactivated");
	}

}
