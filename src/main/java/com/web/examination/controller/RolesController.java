package com.web.examination.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.web.examination.model.Roles;
import com.web.examination.repository.RoleRepository;

@RestController
@RequestMapping(value="/api/roles")
public class RolesController {

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping(value="/findAll")
	public ResponseEntity<List<Roles>> findAll() {
		List<Roles> result = roleRepository.findAll();
		return ResponseEntity.ok().body(result);

	}
	@PostMapping(value="/")
	@RequestMapping(value = "/v1/roles/", method = RequestMethod.POST)
	public ResponseEntity<String> create(@RequestParam Roles role) {
		roleRepository.save(role);
		return ResponseEntity.ok().body("Role Created.!");

	}
	
	@DeleteMapping(value="/remove")
	@RequestMapping(value = "/v1/roles/", method = RequestMethod.DELETE)
	public ResponseEntity<String> remove(@RequestParam String roleName) {
		Roles role=roleRepository.findByRole(roleName);
		role.setActive(false);
		roleRepository.save(role);
		return ResponseEntity.ok().body("Role Deactivated");

	}
	
}
